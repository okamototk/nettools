FROM alpine:latest

RUN apk add --update --no-cache \
  busybox \
  iputils \
  bind-tools \
  net-tools \
  tcpdump \
  curl \
  iperf \
  apache2-utils \
  openldap-clients \
  nmap

CMD ["/bin/sh"]




